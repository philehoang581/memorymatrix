using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Tile : MonoBehaviour
{
    private Action<Tile> _actionOnClick;
    [SerializeField] private Image _targetTile;
    [SerializeField] private Image _wrongTile;
    

    private bool _isCanClick = false;
    private bool _isTarget = false;

    public bool IsTarget
    {
        get
        {
            return _isTarget;
        }
    } 
    public void InitTile(Action<Tile> action)
    { 
        _actionOnClick = action; 
        _targetTile.gameObject.SetActive(false);
    }
    public void ShowTarget()
    {
        _isTarget = true;
        _isCanClick = true;
        _targetTile.gameObject.SetActive(true);
    }
    public void HideTarget()
    {
        _targetTile.gameObject.SetActive(false); 
    }
    public void OnClick()
    {
        
        if (_actionOnClick != null && _isCanClick!=false)
        {
            _isCanClick = false;
            _actionOnClick.Invoke(this);
            _targetTile.gameObject.SetActive(IsTarget);
            _wrongTile.gameObject.SetActive(!IsTarget);
        } 
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "DataLevel", menuName = "ScriptableObjects/LevelConfig", order = 1)]
public class LevelConfig : ScriptableObject
{
    public List<Level> ListLevel;
}

[Serializable]
public class Level
{ 
    public int NumberTarget;
    public int Row;
    public int Column;
}

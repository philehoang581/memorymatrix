using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreTxt;
    [SerializeField] private TextMeshProUGUI _levelTxt;
    public void UpdateLevel(int level) 
    {
        _levelTxt.text = $"Level: {level}";
    }
    public void UpdateScore(int score)
    {
        _scoreTxt.text = $"Score: {score}";
    }
}

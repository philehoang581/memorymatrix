using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BoardManager : MonoBehaviour
{
    private Action<bool> _actionOnSelect; 
    [SerializeField] private Tile _tilePrefab; 
    [SerializeField] private RectTransform _transformBoard;
    private List<Tile> _listTile = new List<Tile>();
    private bool _isCanClick =false; 
    private float _spaceTile = 10;
    private float _widthTile = 150;
    private float _widthScene;

    private Level _level;   
    private void Start()
    {
        _widthScene = Screen.width;
    }
    public bool IsCanClick
    {
        //get { return _isCanClick; }
        set { _isCanClick = value; }
    }
     
    public void SetupBoard(Action<bool> action)
    {
        _actionOnSelect = action;
    }
     
    public void InitBoard(Level level)
    {
        _isCanClick = false;
        _level = level;
        float widthBoard = (_widthTile * level.Row) + (level.Row + 2) * _spaceTile;
        float height = (_widthTile * level.Column) + (level.Column + 2) * _spaceTile; 
        _transformBoard.sizeDelta = new Vector2(widthBoard, height);
        MoveBoard("Start");

        ClearBoard();

        for (int row = 0; row < level.Row; row++)
        {
            for (int column = 0; column < level.Column; column++)
            {
                Tile tile = Instantiate(_tilePrefab, _transformBoard);
                tile.InitTile(TileOnClick);
                _listTile.Add(tile);
            } 
        }

         
    }
    public void MoveBoard(string stages)
    {
        switch (stages)
        {
            case "Start":
                StartCoroutine(MoveStart());
                break;
            case "End":
                StartCoroutine(MoveEnd());
                break;
            default:
                break;
        }    
    }
    private IEnumerator MoveStart()
    {
        //_levelTxt.text = $"Level: {level}";
        _transformBoard.anchoredPosition = new Vector2(_widthScene / 2 + _transformBoard.sizeDelta.x / 2, 0);
        Vector2 centerScene = new Vector2(0, 0);
        float timeMove = 0;
        while (timeMove < 1)
        {
            yield return new WaitForEndOfFrame();
            timeMove += Time.deltaTime;
            _transformBoard.anchoredPosition = Vector2.Lerp(_transformBoard.anchoredPosition, centerScene, timeMove / 16);
        }
        List<int> listTarget = GetListTarget(_level);
        StartCoroutine(ShowTarget(listTarget));
    }
    private IEnumerator MoveEnd()
    {
        //_levelTxt.text = $"Level: {level}";
        //_transformBoard.anchoredPosition = new Vector2(_widthScene / 2 + _transformBoard.sizeDelta.x / 2, 0);
        //Vector2 centerScene = new Vector2(0, 0);
        //float timeMove = 0;
        //while (timeMove < 1)
        //{
        //    yield return new WaitForEndOfFrame();
        //    timeMove += Time.deltaTime;
        //    _transformBoard.anchoredPosition = Vector2.Lerp(_transformBoard.anchoredPosition, centerScene, timeMove / 16);
        //}
        //yield return new WaitForSeconds(0.5f);
        Vector2 leftScene = new Vector2(-(_widthScene / 2 + _transformBoard.sizeDelta.x / 2), 0);
        float timeMoveLeft = 0;
        while (timeMoveLeft < 1)
        {
            yield return new WaitForEndOfFrame();
            timeMoveLeft += Time.deltaTime;
            _transformBoard.anchoredPosition = Vector2.Lerp(_transformBoard.anchoredPosition, leftScene, timeMoveLeft / 16);
        }

        //_showCompletAction?.Invoke();
    }
    private void ClearBoard()
    {
        for (int i = 0; i < _listTile.Count; i++)
        {
            Destroy(_listTile[i].gameObject); 
        }
        _listTile.Clear();
    }
    private IEnumerator ShowTarget(List<int> listTarget)
    {
        for (int i = 0; i < listTarget.Count; i++)
        { 
            _listTile[listTarget[i]].ShowTarget();
        }
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < listTarget.Count; i++)
        { 
            _listTile[listTarget[i]].HideTarget();
        }
        _isCanClick = true;
    }

    private void TileOnClick(Tile tile)
    {
        if (!_isCanClick) return;
        _actionOnSelect.Invoke(tile.IsTarget);
        //Debug.LogError($"TileOnClick: {tile.IsTarget}");
    }
    private List<int> GetListTarget(Level level)
    {
        int numberTarget = level.NumberTarget;
        int maxRange = level.Row * level.Column;
        if (numberTarget > maxRange) throw new ArgumentException("Target cannot be greater than max"); 
        List<int> result = new List<int>();
        for (int i = 0; i < numberTarget; i++)
        {
            int tempNum = 0;
            do
            {
                tempNum = UnityEngine.Random.Range(0, maxRange);
            }
            while (result.Contains(tempNum));
            result.Add(tempNum);
        }
        return result;
    }

}

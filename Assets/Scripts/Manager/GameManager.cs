using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Boder: 322723
    /// Background: 714C42
    /// Tile: 67FEF5
    /// </summary> 
    [SerializeField] private BoardManager _boardManager;
    [SerializeField] private UIManager _uIManager;
    [SerializeField] private LevelConfig _levelConfig;
    [SerializeField] private PopupLevel _popupLevel;
    private int _levelIndex = 1;
    private int _score = 0;
    private int _numberMatch = 0;
    

    private int _step = 0;
    private void Start()
    {
        InitGame();
        StartGame();
    }
    private void StartGame()
    {
        _step = 0;
        //_score = 0;
        _numberMatch = 0;
         
        _uIManager.UpdateLevel(_levelIndex);
        _popupLevel.Show(_levelIndex,
            ()=>
            {
                if (_levelIndex > _levelConfig.ListLevel.Count)
                {
                    throw new System.Exception("Max level"); 
                }
                _boardManager.InitBoard(_levelConfig.ListLevel[_levelIndex]);
            });
    }
    private void InitGame()
    {
        _boardManager.SetupBoard(isTarget=>OnSelectTile(isTarget));
    }

    private void OnSelectTile(bool isTarget)
    {
        if (isTarget)
        {
            _score += 10;
            _numberMatch++;
            _uIManager.UpdateScore(_score);
        }
        else
        {
            _score -= 10; 
            _uIManager.UpdateScore(-_score);
        }
        _step++;
        if (_step >= _levelConfig.ListLevel[_levelIndex].NumberTarget)
        {
            _boardManager.IsCanClick = false;
            _boardManager.MoveBoard("End");
            Debug.LogError($"FINISH LEVEL");
            if (_numberMatch == _levelConfig.ListLevel[_levelIndex].NumberTarget)
            {
                StartCoroutine(NextLevel(1));
            }
            else
            {
                StartCoroutine(NextLevel(-1));
            } 
        } 
    }
    private IEnumerator NextLevel(int level)
    {
        yield return new WaitForSeconds(1f);
        _levelIndex += level;
        if (_levelIndex <= 0) _levelIndex = 1;
        StartGame(); 
    }
#if UNITY_EDITOR
    public void TestNextlevel()
    {
        _levelIndex ++;
        if (_levelIndex <= 0) _levelIndex = 1;
        StartGame();
    }
#endif
}

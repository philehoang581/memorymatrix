using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class PopupLevel : MonoBehaviour
{
    private Action _showCompletAction;
    [SerializeField] private RectTransform _rectTransform;
    [SerializeField] private TextMeshProUGUI _levelTxt;
    private float _widthScene;

    private void Start()
    {
        _widthScene = Screen.width; 
    }
    public void Show(int level, Action actionComplet)
    {
        _showCompletAction = actionComplet;
        StartCoroutine(IEShow(level));
    }
    private IEnumerator IEShow(int level)
    {
        _levelTxt.text = $"Level: {level}";
        _rectTransform.anchoredPosition = new Vector2(_widthScene/2 + _rectTransform.sizeDelta.x/2, 0); 
        Vector2 centerScene = new Vector2(0, 0); 
        float timeMove = 0;
        while (timeMove < 1)
        {
            yield return new WaitForEndOfFrame();
            timeMove += Time.deltaTime;
            _rectTransform.anchoredPosition = Vector2.Lerp(_rectTransform.anchoredPosition,centerScene, timeMove/16);
        } 
        yield return new WaitForSeconds(0.5f); 
        Vector2 leftScene = new Vector2(-(_widthScene / 2 + _rectTransform.sizeDelta.x / 2), 0);
        float timeMoveLeft = 0;
        while (timeMoveLeft < 1)
        {
            yield return new WaitForEndOfFrame();
            timeMoveLeft += Time.deltaTime;
            _rectTransform.anchoredPosition = Vector2.Lerp(_rectTransform.anchoredPosition, leftScene, timeMoveLeft / 16);
        }

        _showCompletAction?.Invoke();
    }
}
